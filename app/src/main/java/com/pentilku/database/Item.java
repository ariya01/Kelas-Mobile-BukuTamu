package com.pentilku.database;

/**
 * Created by Ariya on 05/12/2017.
 */

public class Item {
    private String alamat;
    private String nama;
    public  Item (String nama,String Alamat)
    {
        this.nama=nama;
        this.alamat=Alamat;
    }


    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }


}
