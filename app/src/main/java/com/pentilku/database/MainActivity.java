package com.pentilku.database;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class MainActivity extends AppCompatActivity {
    private SQLiteDatabase dbku;
    private SQLiteOpenHelper Opendb;
    private Button btn_simpan,btn_ambil,btn_update,btn_delete,btn_tanggal,btn_waktu,btn_pindah,btn_fb,btn_list;
    private EditText et_nama,et_alamat,et_waktu,et_pesan;
    private TextView tv_tanggal,tv_waktu;
    Calendar mdate;
    int day,month,year,hour,minute;
    Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv_waktu = (TextView)findViewById(R.id.tv_waktu);
        tv_tanggal = (TextView)findViewById(R.id.tv_tanggal);
        btn_ambil = (Button)findViewById(R.id.btn_ambil);
        btn_simpan = (Button)findViewById(R.id.btn_simpan);
        et_nama = (EditText) findViewById(R.id.et_nama);
        et_alamat = (EditText)findViewById(R.id.et_alamat);
        et_pesan =(EditText)findViewById(R.id.et_pesan);
        btn_tanggal = (Button)findViewById(R.id.btn_tanggal);
        btn_delete = (Button)findViewById(R.id.btn_delete);
        btn_update = (Button)findViewById(R.id.btn_update);
        btn_waktu = (Button)findViewById(R.id.btn_waktu);
        btn_pindah = (Button)findViewById(R.id.btn_pindah);
        btn_fb = (Button)findViewById(R.id.btn_fb);
        btn_simpan.setOnClickListener(operasi);
        btn_ambil.setOnClickListener(operasi);
        btn_update.setOnClickListener(operasi);
        btn_delete.setOnClickListener(operasi);
        btn_tanggal.setOnClickListener(operasi);
        btn_waktu.setOnClickListener(operasi);
        btn_pindah.setOnClickListener(operasi);
        btn_fb.setOnClickListener(operasi);
        btn_list= (Button)findViewById(R.id.masuknya);
        btn_list.setOnClickListener(operasi);

        mdate = Calendar.getInstance();
        day = mdate.get(Calendar.DAY_OF_MONTH);
        month = mdate.get(Calendar.MONTH);
        year = mdate.get(Calendar.YEAR);
        month = month+1;
        hour = mdate.get(Calendar.HOUR);
        minute = mdate.get(Calendar.MINUTE);

        Opendb = new SQLiteOpenHelper(this,"db.sql",null,1) {
            @Override
            public void onCreate(SQLiteDatabase db) {

            }

            @Override
            public void onUpgrade(SQLiteDatabase db, int oldVersion , int newVersion) {

            }
        };
        dbku = Opendb.getWritableDatabase();
        dbku.execSQL("create table if not exists tamu(nama TEXT,waktu TEXT, alamat TEXT, tanggal DATE, pesan TEXT);");
//        ambildata();
    }

    View.OnClickListener operasi = new View.OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void onClick(View view) {
            switch (view.getId())
            {
                case R.id.btn_simpan:simpan();break;
                case R.id.btn_ambil:ambil();break;
                case R.id.btn_delete:hapus();break;
                case R.id.btn_update:update();break;
                case R.id.btn_tanggal:tanggal();break;
                case R.id.btn_waktu:waktu();break;
                case R.id.btn_pindah:pindah();break;
                case R.id.btn_fb:face();break;
                case R.id.masuknya:masuk();break;
            }
        }
    };

    private void masuk() {
        Intent intent = new Intent(MainActivity.this,Main3Activity.class);
        MainActivity.this.startActivity(intent);
    }

    private void face()
    {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com"));
        startActivity(intent);
    }

    private void pindah()
    {
        Intent intent = new Intent(MainActivity.this,Main2Activity.class);
        intent.putExtra("nama",et_nama.getText().toString());
        MainActivity.this.startActivity(intent);
    }

    private void waktu()
    {
        TimePickerDialog waktunya = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hour, int minutes) {
                tv_waktu.setText(hour+":"+minutes);
            }
        },hour,minute,false);
        waktunya.show();
        Toast.makeText(this, "Masukan Waktu Datang", Toast.LENGTH_SHORT).show();
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void tanggal()
    {
        DatePickerDialog cari = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int y, int i1, int i2) {
                i1+=1;
                tv_tanggal.setText(i2+"/"+i1+"/"+y);
            }
        },year,month,day);
        cari.show();
        Toast.makeText(this, "Masukan Tanggal Datang", Toast.LENGTH_SHORT).show();

    }
    private void simpan()
    {
        if(et_nama.getText().toString().equals(""))
        {
            et_nama.setError("Nama Kosong");
        }
        if(et_alamat.getText().toString().equals(""))
        {
            et_alamat.setError("Alamat Kosong");
        }
        if (tv_tanggal.getText().toString().equals("Tanggal Belum Dipilih"))
        {
            tv_tanggal.setError("Tanggal Kosong");
        }
        if (et_pesan.getText().toString().equals(""))
        {
            et_pesan.setError("Pesan Kosong");
        }

        if(!et_nama.getText().toString().equals("")&& !et_alamat.getText().toString().equals("") && !tv_tanggal.getText().toString().equals("Tanggal Belum Dipilih")&& !et_pesan.getText().toString().equals("")){
            ContentValues dataku = new ContentValues();
            dataku.put("nama",et_nama.getText().toString());
            dataku.put("alamat",et_alamat.getText().toString());
            dataku.put("pesan",et_pesan.getText().toString());
            dataku.put("tanggal",tv_tanggal.getText().toString());
            dataku.put("waktu",tv_waktu.getText().toString());
            dbku.insert("tamu",null,dataku);
            Toast.makeText(this, "Data Masuk", Toast.LENGTH_SHORT).show();
        }
    }

    private void ambil()
    {
        Cursor cur =dbku.rawQuery("select * from tamu where nama='" + et_nama.getText().toString()+ "'",null);
        if (cur.getCount()>0)
        {
            Toast.makeText(this, "Data Ditemukan Sejumlah"+cur.getCount(), Toast.LENGTH_SHORT).show();
            cur.moveToFirst();
            et_nama.setText(cur.getString(cur.getColumnIndex("nama")));
            et_alamat.setText(cur.getString(cur.getColumnIndex("alamat")));
            et_pesan.setText(cur.getString(cur.getColumnIndex("pesan")));
            tv_tanggal.setText(cur.getString(cur.getColumnIndex("tanggal")));
            tv_waktu.setText(cur.getString(cur.getColumnIndex("waktu")));
        }
        else
        {
            Toast.makeText(this, "Data Tidak ditemukan", Toast.LENGTH_SHORT).show();
        }
    }

    private void hapus()
    {
        dbku.delete("tamu","nama='"+et_nama.getText().toString()+"'",null);
        Toast.makeText(this,"Data Terhapus",Toast.LENGTH_LONG).show();
    }

    private void update()
    {
        ContentValues dataku = new ContentValues();
        dataku.put("nama",et_nama.getText().toString());
        dataku.put("alamat",et_alamat.getText().toString());
        dataku.put("pesan",et_pesan.getText().toString());
        dataku.put("tanggal",tv_tanggal.getText().toString());
        dataku.put("waktu",tv_waktu.getText().toString());
        dbku.update("tamu",dataku,"nama='"+et_nama.getText().toString()+"'",null);
        Toast.makeText(this,"Data Terupdate",Toast.LENGTH_LONG).show();
    }

    private void inserttamu(String nama,String alamat)
    {
        Item newitem = new Item(nama,alamat);
        adapter.add(newitem);
    }

    public void ambildata()
    {
        Cursor cursor = dbku.rawQuery("select * from tamu",null);
        Toast.makeText(this, "Terdapat" + cursor.getCount(), Toast.LENGTH_SHORT).show();
        int i=0;
        if (cursor.getCount()>0)
        {
            cursor.moveToFirst();
        }
        while (i<cursor.getCount())
        {
            inserttamu(cursor.getString(cursor.getColumnIndex("nama")),cursor.getString(cursor.getColumnIndex("alamat")));
            cursor.moveToNext();
            i++;
        }
    }
}
