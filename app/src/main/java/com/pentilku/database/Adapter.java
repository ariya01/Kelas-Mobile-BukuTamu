package com.pentilku.database;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ariya on 05/12/2017.
 */

public class Adapter extends ArrayAdapter<Item> {

    public Adapter(@NonNull Context context, int resource, @NonNull List<Item> objects) {
        super(context, resource, objects);
    }

    public View getView (int position, View ConvertView, ViewGroup parent)
    {
        Item dtitem =getItem(position);
        if (ConvertView==null)
        {
            ConvertView = LayoutInflater.from(getContext()).
                    inflate(R.layout.custom_list_item,parent,false);
        }
        TextView tAlamat = (TextView)ConvertView.findViewById(R.id.barualamat);
        TextView tNama = (TextView)ConvertView.findViewById(R.id.barunama);

        tAlamat.setText(dtitem.getAlamat());
        tNama.setText(dtitem.getNama());
        return ConvertView;
    }

}
