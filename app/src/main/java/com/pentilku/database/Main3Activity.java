package com.pentilku.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

public class Main3Activity extends AppCompatActivity {
    private ListView listView;
    private ArrayAdapter<String> item;
    private SQLiteDatabase dbku;
    private SQLiteOpenHelper Opendb;
    private Adapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        listView = (ListView)findViewById(R.id.hehe);
        ArrayList<Item> list = new ArrayList<Item>();
        adapter = new Adapter(this,0,list);
        listView.setAdapter(adapter);
        Opendb = new SQLiteOpenHelper(this,"db.sql",null,1) {
            @Override
            public void onCreate(SQLiteDatabase db) {

            }

            @Override
            public void onUpgrade(SQLiteDatabase db, int oldVersion , int newVersion) {

            }
        };
        dbku = Opendb.getWritableDatabase();
        dbku.execSQL("create table if not exists tamu(nama TEXT,waktu TEXT, alamat TEXT, tanggal DATE, pesan TEXT);");
        ambildata();

//        Item newitem1 = new Item("Ariya","Perumdos");
//        Item newiteem2 = new Item("Pentil","sini");
//        adapter.add(newitem1);
//        adapter.add(newiteem2);
//        String [] nama = new  String[] {"ariya","wildan","pentil"};
//        ArrayList<String> list = new ArrayList<>();
//        list.addAll(Arrays.asList(nama));
//
//        item = new ArrayAdapter<String>(this,R.layout.custom_list_item,R.id.nama,list);
//        listView.setAdapter(item);

    }
    private void inserttamu(String nama,String alamat)
    {
        Item newitem = new Item(nama,alamat);
        adapter.add(newitem);
    }

    public void ambildata()
    {
        Cursor cursor = dbku.rawQuery("select * from tamu",null);
        Toast.makeText(this, "Terdapat" + cursor.getCount(), Toast.LENGTH_SHORT).show();
        int i=0;
        if (cursor.getCount()>0)
        {
            cursor.moveToFirst();
        }
        while (i<cursor.getCount())
        {
            inserttamu(cursor.getString(cursor.getColumnIndex("nama")),cursor.getString(cursor.getColumnIndex("alamat")));
            cursor.moveToNext();
            i++;
        }
    }
}
